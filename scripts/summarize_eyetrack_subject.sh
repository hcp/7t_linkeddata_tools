#!/bin/bash

set -e

#########################################################
function show_help {
	echo "

"`basename $0`" <subject> [<rootdir>]

	For a given subject, summarize the eyetracking status for each scan type.
	Scan has 'complete' eyetracking if TRfrac >= 100 and eyefrac >= 75

	subject			subject ID
	scantype		REST,RET,MOV
	scantype_complete	0,1 = were ALL scans for this scantype complete?
	scans_complete		string of 0,1 for EACH scan of this type 
				  (eg: for RET, 011111 means all but RET1 were complete)
	min_duration		minimum TRfrac for this scantype
	min_eyefrac		minimum eyefrac for this scantype

	"
	exit 0
} 

function min {
	a=$1
	b=`echo $2 $1 | awk '{print $1}'`
	echo "if($a < $b){ $a } else { $b }" | bc
	exit 0
} 

function fieldidx {
	echo ${2} | tr "," "\n" | grep -nm1 "^${1}\$" | cut -d: -f1
	exit 0
} 
#########################################################

if [ $# -lt 1 ]; then
	show_help;
	exit 0
fi

subject=$1
scantypes=(REST MOV RET)

behavdir=/home/range1-raid1/kjamison/range3extra/eyelink
if [ $# -gt 2 ]; then
	behavdir=$2
fi

subjdir=${behavdir}/${subject}

#scan is valid if TRfrac >= 100% and eyefrac >= 75%
eyefield=eyefrac
durfield=TRfrac
eyeval_min=75
durval_min=100

subjectsummaryfile="${behavdir}/${subject}/${subject}_summary.csv"
rm -f ${subjectsummaryfile}

printf "subject,scantype,scantype_complete,scans_complete,min_duration,min_eyefrac\n" >> ${subjectsummaryfile}

for st in ${scantypes[@]}; do

	case ${st} in
		REST) numscans=4;;
		MOV) numscans=4;;
		RET) numscans=6;;
	esac		

	scantype_eyemin=
	scantype_durmin=
	validstr=
	for sc in `seq 1 ${numscans}`; do
		trfrac=0
		eyefrac=0
		isvalid=0
		ascfile=${subjdir}/${subject}_7T_${st}${sc}_eyetrack_summary.asc
		csvfile=${subjdir}/${subject}_7T_${st}${sc}_eyetrack_summary.csv

		#if no csv file present, all values are 0
		if [ -e ${csvfile} ]; then

			#determine which columns of csv to read
			hdrline=`head -n1 $csvfile`
			trfracidx=`fieldidx ${durfield} "${hdrline}"`
			eyefracidx=`fieldidx ${eyefield} "${hdrline}"`

			#if columns are missing, all values are 0
			if [ "x${trfracidx}" = "x" ] || [ "x${eyefracidx}" = "x" ]; then
				#missing required fields
				isvalid=0
			else
				#read values, 0 if blank
				trfrac=`tail -n +2 $csvfile | head -n1 | cut -d, -f${trfracidx}`
				eyefrac=`tail -n +2 $csvfile | head -n1 | cut -d, -f${eyefracidx}`
				trfrac=`echo $trfrac 0 | awk '{print $1}'`
				eyefrac=`echo $eyefrac 0 | awk '{print $1}'`

				#scan is valid if all vals exceed thresholds 
				trvalid=`echo "${trfrac} >= ${durval_min}" | bc`
				eyevalid=`echo "${eyefrac} >= ${eyeval_min}" | bc`
				if [[ ${trvalid} == 1 ]] && [[ ${eyevalid} == 1 ]]; then
					isvalid=1
				fi
			fi

		fi
		scantype_eyemin=`min ${scantype_eyemin} ${eyefrac}`
		scantype_durmin=`min ${scantype_durmin} ${trfrac}`
		validstr=${validstr}${isvalid}
	done
	#scantype is valid if all scans pass these thresholds
	durvalid=`echo "${scantype_durmin} >= ${durval_min}" | bc`
	eyevalid=`echo "${scantype_eyemin} >= ${eyeval_min}" | bc`
	scantype_isvalid=0
	if [[ ${durvalid} == 1 ]] && [[ ${eyevalid} == 1 ]]; then
		scantype_isvalid=1
	fi

	printf "%s,%s,%s,%s,%s,%s\n" \
		${subject} \
		${st} \
		${scantype_isvalid} \
		${validstr} \
		${scantype_durmin} \
		${scantype_eyemin} >> ${subjectsummaryfile}
done


echo 
echo "${subjectsummaryfile}:"
cat ${subjectsummaryfile}
echo
