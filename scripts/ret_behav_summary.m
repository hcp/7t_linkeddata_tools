function ret_summary = ret_behav_summary(matfile)

% function ret_summary = ret_behav_summary(matfile)
% 
% <matfile> is a string referring to a behavioral .mat file
%
% this function returns a struct with information extracted from the .mat file.  
% the idea is that we want to strip unnecessary and unwanted information from
% the .mat file.

% the relevant fields from the .mat file are:
% - 'setnum' is an integer 89-93.  these correspond to run types 1-5.
% - 'frameorder' is 2x4500.  the first row indicates stochastic object frames used
%    (0-100); the second row indicates the mask frames used.  the first row is necessary
%    to reconstruct what the actual physical stimulus was.  the second row is
%    already conveyed by the stimulus movie files.
% - 'fixationorder' is 1x4503.  this is a vector such that elements (2:end-2) 
%    indicate the color of the fixation dot.
% - 'timeframes' is 1x4500.  this indicates the time that each frame was shown.
%    ideally, frames are shown without any missed frames, in which case 'timeframes'
%    will be a linearly increasing series of values.  but if there are missed frames,
%    this will be reflected in the values in 'timeframes'.
% - 'timekeys' is an Nx2 cell vector that contains the times of buttons and TTL pulses.
% - 'specialoverlay' is the fixation grid that overlays the stimuli.  we don't actually
%    output this, but it may be of interest in the future.

%%%%%%%%%%%%%%% load .mat file and perform sanity checks

% load .mat file
if(ischar(matfile) && exist(matfile,'file'))
    load(matfile,'setnum','frameorder','fixationorder','timeframes','timekeys');
else
  disp(matfile);
  error('bad input');
end
% elseif(isstruct(matfile))
%     M = matfile;
% else
%     fprintf('bad input to %s:\n',justfilename(mfilename));
%     disp(matfile);
%     error('bad input');
% end

% sanity check the information
assert(isequal(size(setnum),[1 1]) && setnum >= 89 && setnum <= 93);
assert(isequal(size(frameorder),[2 4500]));
assert(isequal(size(fixationorder),[1 4503]));
assert(isequal(size(timeframes),[1 4500]));
assert(size(timekeys,1) > 0);

% sanity check the total duration of the stimulus
totaldur = length(timeframes) * mean(diff(timeframes));
assert(totaldur >= 299 && totaldur <= 301);

%%%%%%%%%%%%%%% pre-process the timekeys

% expand the multiple-keypress cases [this creates timekeysB]
timekeysB = {};
for p=1:size(timekeys,1)
  if iscell(timekeys{p,2})
    for pp=1:length(timekeys{p,2})
      timekeysB{end+1,1} = timekeys{p,1};
      timekeysB{end,2} = timekeys{p,2}{pp};
    end
  else
    timekeysB(end+1,:) = timekeys(p,:);
  end
end

%%%%%%%%%%%%%%% pre-process the TTL pulses, button presses, and dot events

% extract TTL timepoints [ttltimes is a vector of times]
deltatime=.1;  % TTL entries with less than this differential are counted as the same pulse
ttltriggers=[timekeysB{cellfun(@(x)(x(1)=='5' | strcmp(x,'trigger')),timekeysB(:,2)),1}];  % vector of times
if isempty(ttltriggers)
  ttltimes = [];  % this crazy case might happen
else
  ttltimes=ttltriggers([true diff(ttltriggers)>deltatime]);  % extract times that are greater than the delta
      % %time0 = ttltimes(1);
      % time0=[timekeysB{cellfun(@(x)(strcmp(x,'absolutetimefor0')),timekeysB(:,2)),1}];
end

% figure out when the user pressed a button [buttontimes_save is a vector of times]
deltatime = 1;  % holding the key down for less than this time will be counted as one button press
oldkey = ''; oldkeytime = -Inf;
buttontimes = [];
for p=1:size(timekeysB,1)

  % is this a bogus key press?
  bad = isequal(timekeysB{p,2},'absolutetimefor0') | ...
        isequal(timekeysB{p,2},'trigger') | ...
        isequal(timekeysB{p,2}(1),'5') | ...
        (isequal(timekeysB{p,2},oldkey) & timekeysB{p,1}-oldkeytime <= deltatime);
  
  % if not bogus, then record the button time
  if ~bad
    buttontimes = [buttontimes timekeysB{p,1}];
    oldkey = timekeysB{p,2};
    oldkeytime = timekeysB{p,1};
  end

end
buttontimes_save = buttontimes;   % we mangle buttontimes below, so we need to save the original

% figure out when the dot changed colors [changetimes is a vector of times]
seq = abs(diff(fixationorder(2:end-2))) > 0;
changetimes = timeframes(find(seq) + 1);

%%%%%%%%%%%%%%% analyze the behavioral responses

% define
firstel=@(x)(x(1));

% calc
numtot = length(changetimes);    % total number of color changes

% figure out the number of hits [numhits is an integer]
numhits = 0;
respondtime = 1;  % the subject has this much time to press a button
hitsmisses = zeros(size(timeframes));  % 1 x frames
for q=1:length(changetimes)
  [dd,frameidx] = min(abs(timeframes-changetimes(q)));  % frameidx is the frame that the change occurred
  okok = (buttontimes > changetimes(q)) & (buttontimes <= changetimes(q) + respondtime);  % indices of buttons pressed
  if any(okok)
    hitsmisses(frameidx) = 1;   % 1 means hit
    numhits = numhits + 1;
    buttontimes(firstel(find(okok))) = [];  % remove!
  else
    hitsmisses(frameidx) = -1;  % -1 means miss
  end
end

% figure out the number of false alarms [numfalse is an integer]
numfalse = length(buttontimes);
for b = 1:length(buttontimes)
    [dd,frameidx] = min(abs(timeframes-buttontimes(b)));
    hitsmisses(frameidx) = -2;  % -2 means false alarm
end

% calc
retscore=(numhits-numfalse)/numtot*100;         % a percentage that summarizes performance
hittimes=timeframes(hitsmisses==1);             % vector of all color change times that were hits
misstimes=timeframes(hitsmisses==-1);           % vector of all color change times that were misses
falsetimes=buttontimes;                         % vector of all button times that were false alarms

%%%%%%%%%%%%%%%%%%
expttype_str = {'CCW','CW','EXP','CON','BAR'};
expttype=setnum-88;
%%%%%%%%%%%%%%% prepare output

ret_summary = struct( ...
  'expttype',expttype, ...                % which experiment type (1=CCW, 2=CW, 3=expand, 4=contract, 5=bar)
  'exptname',expttype_str{expttype}, ...  % 
  'ttlStamps',ttltimes, ...               % vector of TTL times (might be empty if there was a problem)
  'changetimes',changetimes, ...          % vector of all times that the dot changed color
  'buttontimes',buttontimes_save, ...     % vector of all times that the subject pressed a button (might be empty if there was a problem)
  'numtot',numtot, ...                    % positive integer with total number of color changes
  'numhits',numhits, ...                  % integer with number of hits
  'nummiss',numtot-numhits, ...           % integer with number of misses
  'numfalse',numfalse, ...                % integer with number of false alarms
  'score',retscore, ...                   % summary of performance (percentage)
  'hitmissSequence',hitsmisses, ...       % a vector indicating for each frame whether it was a hit (1), miss (-1), or false alarm (-2).
  ...                                     % the hits and misses are marked according to the frame of the color change.
  ...                                     % the false alarms are marked according to the temporally closest frame.
  'hittimes',hittimes, ...                % vector of color change times that were hits
  'misstimes',misstimes, ...              % vector of color change times that were misses
  'falsetimes',falsetimes, ...            % vector of all button times that were false alarms
  'patternindices',frameorder(1,:), ...   % vector of indices of which pattern was shown on each frame
  'dotcolors',-fixationorder(2:end-2), ...% vector indicating the color of the fixation dot on each frame (1=red,2=black,3=white)
  'timeframes',timeframes);               % vector indicating time of presentation of each frame

% note that time=0 corresponds to the time of the first frame.  all times are in seconds.
