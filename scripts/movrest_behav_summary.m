function mat_summary = movrest_behav_summary(matfile)

% function mat_summary = movrest_behav_summary(matfile)
% 
% <matfile> is a string referring to a behavioral .mat file
%
% this function loads and parses <matfile> and returns a structure
% summarizing the timing information it contains
% For MOVIE and RESTING scans, the output should contain:
%   ttlStamps, ttlCnt, timeStart, timeEnd, duration
% For more recent scans (after 2015-01-23), <matfile> also contains
%   ttlStart, timeMovieEnd, ttlFrames
%
% Output structure contains:
% mat_summary.ttlStamps = timestamps of TTL from scanner, relative to start
%   of stimulus
% mat_summary.ttlCnt = number of timestamps recorded
% mat_summary.duration = total duration of stimulus
% mat_summary.ttlFrames = timestamps for each frame of the movie, relative
%   to start of the stimulus.  If not present in <matfile>, return NaN

if(ischar(matfile) && exist(matfile,'file'))
    M=load(matfile);
else
  disp(matfile);
  error('bad input');
end

if(numel(fieldnames(M))==1 && isfield(M,'movInfo'))
    M = M.movInfo;
end

assert(isfield(M,'ttlStamps') && isfield(M,'ttlCnt') && isfield(M,'duration'));

mat_summary = struct();

%only contains ttlStamps, ttlCnt, timeStart, timeEnd, duration
%timeStart defined as = ttlStamps(1)
%timeEnd = when movie finishes (1-2 seconds after last ttl)
%duration = timeEnd-timeStart
%so only care about ttlStamps and duration
%subtract timeStart from ttlStamps to avoid possible date info
mat_summary.ttlStamps = M.ttlStamps(1:M.ttlCnt)-M.ttlStamps(1);
mat_summary.ttlCnt = M.ttlCnt;
mat_summary.duration = M.duration;

%additional fields stored as of 2015-01-23
%ttlStart,timeMovieEnd,ttlFrames
if(isfield(M,'ttlFrames'))
    %stored 0 for initial ttl (since it hasn't started playing)
    mat_summary.ttlFrames = M.ttlFrames(1:M.ttlCnt);
else
    mat_summary.ttlFrames = nan;
end
