#!/bin/bash

SCRIPTDIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
for matfile in $@
do
	echo "hcp_behav2xml ${matfile}"
	matlab -nodesktop -nosplash -r "try, addpath('${SCRIPTDIR}'); hcp_behav2xml('${matfile}'); catch e, fprintf('Error:%s\n',e.message), end, quit"
done

