#!/bin/bash

function display_help {
echo "
 "`basename $0`" [-n] <eyetrackfile>.asc [<summaryfile>]
	default output: <eyetrackfile>_summary.csv

 Parse through eyetracker .asc output and print CSV file with summary information:
	subject		subject ID
	scan		REST1, RET3, MOV2, etc...
	block 		recording block index (should only be 1, but may have additional)
	failsafe 	0,1 = Was -failsafe flag used in converting edf file?
	startline 	where this recording block begins (START event)
	endline 	where this recording block begins (END event)
	numlines 	total lines for this START->END block
	srate 		sampling rate (Hz)
	durexpected	expected duration for this scan (scan type determined from filename)
	duration	duration (in seconds) of this recording block
	durfrac		100 * (duration / expected duration)
	eyefrac		100 * ( samples with eye position / total samples ). Percentage where eye was detected
	TRcount		Number of scanner volume triggers sent from stimulus code (MSG TRIALID events)
	TRonset		Time (in seconds) of first TR relative to block START
	TRduration	Time (in seconds) from first TR until block END
	TRfrac		100 * ( TRduration / expected duration )

 Note: TR events were only sent to eyetracker during MOVIE and REST scans
       for RET scans, only ENDRET is sent, from which we can infer the first TR was 300 seconds earlier

 If file contains more than one recording block, automatically select one based on <selectfield>
 Optional argument -n: skip automatic selection
"
	exit 0

}

function transp {
	awk '
	{ 
	    for (i=1; i<=NF; i++)  {
		a[NR,i] = $i
	    }
	}
	NF>p { p = NF }
	END {    
	    for(j=1; j<=p; j++) {
		str=a[1,j]
		for(i=2; i<=NR; i++){
		    str=str" "a[i,j];
		}
		print str
	    }
	}' $@
}

if [ $# -eq 0 ]; then
	display_help;
fi

#################################################
noselect=
if [ "$1" = "-n" ]; then
	noselect=1
	shift;
fi

ascfile=$1
summaryfile=$2
selectfield="duration"


##################################################
# parse filename for subject and scantype
subjname=`basename $ascfile | sed -En 's/(^[^_]+)_.+$/\1/p'`
scanname=`basename $ascfile | sed 's/_FIX/_REST/g' | sed -E 's/(REST|MOV|RET)eyetrack/\1/' | sed -En 's/^.+_(REST[1234]|MOV[1234]|RET[123456])_.+$/\1/p'`

case ${scanname} in
	MOV1) expected_duration=921;;
	MOV2) expected_duration=918;;
	MOV3) expected_duration=915;;
	MOV4) expected_duration=901;;
	RET*) expected_duration=300;;
	REST*) expected_duration=900;;
	*) expected_duration=;;
esac		

##################################################
if [ "x$summaryfile" = "x" ]; then
	summaryfile=`echo $ascfile | sed 's/\.asc$/_summary.csv/'`
fi
rm -f ${summaryfile}

isfailsafe=`head -n10 ${ascfile} | grep -Em1 '^\*\* CONVERTED' | grep "failsafe" | wc -l`

startlines=`grep -nE '^START\s' ${ascfile} | awk '{printf "%s:%s\n",$1,$2}'`
endlines=`grep -nE '^END\s' ${ascfile} | awk '{printf "%s:%s\n",$1,$2}'`

filelines=`wc -l ${ascfile} | awk '{print $1}'`

numstart=`echo $startlines | wc -w`

if [ ${numstart} = 0 ]; then
	echo "No START events.  Invalid file: ${ascfile}"
	exit 0
fi

#################################################
printf "subject,scan,block,failsafe,startline,endline,numlines,srate,durexpected,duration,durfrac,eyefrac,TRcount,TRonset,TRduration,TRfrac\n" >> ${summaryfile}

#################################################
for i in `seq 1 ${numstart}`; do
	s_str=`echo ${startlines} | cut -d" " -f$i`
	e_str=`echo ${endlines} | cut -d" " -f$i`
	if [ "x${s_str}" = "x" ]; then
		echo "no start?"
	fi
	s_line=`echo ${s_str} | cut -d: -f1`
	s_time=`echo ${s_str} | cut -d: -f3`

	#if there is no END event (file partially corrupted... likely required conversion in failsafe mode)
	# use the last line of eyetracking data as the "END" for the purpose of this summary
	if [ "x${e_str}" = "x" ]; then
		echo "START$i(failsafe=${isfailsafe}) missing END for ${s_str}"
		#continue

		laststr=`tail -n +${s_line} ${ascfile} | grep -nE '^[0-9]' | tail -n1`
		lastline=`echo ${laststr} | awk '{print $1}' | cut -d: -f1`
		lasttime=`echo ${laststr} | awk '{print $1}' | cut -d: -f2`

		lastline=`echo "${lastline} + ${s_line} - 1" | bc`
		e_str="${lastline}:END:${lasttime}"
	fi

	e_line=`echo ${e_str} | cut -d: -f1`
	e_time=`echo ${e_str} | cut -d: -f3`
	
	numline=`echo "${e_line} - ${s_line} + 1" | bc`
	dur=`echo "${e_time} - ${s_time}" | bc`

	#find sampling rate
	#NOTE: Timestamps in file are in milliseconds, not samples, so do NOT use this rate for comparisons later
	rate=`tail -n +${s_line} ${ascfile} | head -n ${numline} | grep -Em1 '^SAMPLES' | sed -E 's/^.+[[:space:]]RATE[[:space:]]+([0-9\.]+)[[:space:]].+$/\1/'`

	#find scanner TTL events sent from matlab
	ttlmsg=`tail -n +${s_line} ${ascfile} | head -n ${numline} | grep -E '^MSG.+TRIALID' | awk '{printf "%s:%s\n",$2,$4}'`
	numttl=`echo $ttlmsg | wc -w`
	stimulus_onset=
	ttl1_to_end=

	if [ ! ${numttl} = 0 ]; then
		ttl1=`echo $ttlmsg | cut -d" " -f1`
		ttl1_idx=`echo ${ttl1} | cut -d: -f2`

		ttl1_time=`echo ${ttl1} | cut -d: -f1`
		ttl1_to_end=`echo "${e_time} - ${ttl1_time}" | bc`
		
		if [ ${ttl1_idx} = "1" ]; then
			stimulus_onset=`echo "${ttl1_time} - ${s_time}" | bc`
		fi
	fi

	#find ENDRET event sent from matlab
	#If present, subtract 300seconds from ENDRET to determine approx. stimulus onset time (since no message is
	# sent at start of retinotopy stimulus)
	endret_time=`tail -n +${s_line} ${ascfile} | grep -Em1 '^MSG.+ENDRET' | awk '{print $2}'`
	if [ ! "x${endret_time}" = "x" ] ; then
		retdur=${expected_duration}
		startret=`echo "${endret_time} - 1000*${retdur}" | bc`
		stimulus_onset=`echo "${startret} - ${s_time}" | bc`
		ttl1_to_end=`echo "1000*${retdur}" | bc`
	fi

	#Compute percentage time between START and END where eye position was actually recorded
	timepoints=`tail -n +${s_line} ${ascfile} | head -n ${numline} | grep -E '^[0-9]' | wc -l`
	trackpoints=`tail -n +${s_line} ${ascfile} | head -n ${numline} | grep -E '^[0-9]' | awk '{print $2}' | grep '[0-9]' | wc -l`
	trackfrac=`echo "scale=3; 100*${trackpoints}/${timepoints}" | bc`

	duration_sec=
	stimulus_onset_sec=
	ttl2end_sec=
	ttl2end_frac=

	if [ ! "x${dur}" = "x" ]; then
		duration_sec=`echo "scale=3; ${dur}/1000" | bc`
	fi
	if [ ! "x${duration_sec}" = "x" ] && [ ! "x${expected_duration}" = "x" ]; then
		duration_frac=`echo "scale=3; 100*${duration_sec}/${expected_duration}" | bc`
	fi
	if [ ! "x${stimulus_onset}" = "x" ]; then
		stimulus_onset_sec=`echo "scale=3; ${stimulus_onset}/1000" | bc`
	fi
	if [ ! "x${ttl1_to_end}" = "x" ]; then
		ttl2end_sec=`echo "scale=3; ${ttl1_to_end}/1000" | bc`
	fi
	if [ ! "x${ttl2end_sec}" = "x" ] && [ ! "x${expected_duration}" = "x" ]; then
		ttl2end_frac=`echo "scale=3; 100*${ttl2end_sec}/${expected_duration}" | bc`
	fi
	printf "%s,%s,%d,%d,%d,%d,%d,%.0f,%s,%s,%s,%s,%d,%s,%s,%s\n" \
		"${subjname}" \
		"${scanname}" \
		"${i}" \
		"${isfailsafe}" \
		"${s_line}" \
		"${e_line}" \
		"${numline}" \
		"${rate}" \
		"${expected_duration}" \
		"${duration_sec}" \
		"${duration_frac}" \
		"${trackfrac}" \
		"${numttl}" \
		"${stimulus_onset_sec}" \
		"${ttl2end_sec}" \
		"${ttl2end_frac}" >> ${summaryfile}

done

echo 
echo "${summaryfile}:"
cat ${summaryfile}
echo
cat ${summaryfile} | tr "," " " | transp | awk '{printf "%-15s %-15s\n",$1,$2}'
echo

##########################################################
##### check for and handle file with multiple recording blocks

summarylines=`cat ${summaryfile} | wc -l`
summarylines=$(( summarylines - 1 ))

if [[ "x${noselect}" = "x" ]] && [[ ${summarylines} > 1 ]]; then

	#create backups of original .asc and summary.csv file
	ascbackup="${ascfile}.bak"
	sumbackup="${summaryfile}.bak"
	mv -f ${ascfile} ${ascbackup}
	mv -f ${summaryfile} ${sumbackup}

	#make sure we are reading the correct csv field
	selectidx=`cat ${sumbackup} | head -n 1 | tr "," "\n" | grep -nm1 "^${selectfield}\$" | cut -d: -f1`
	startidx=`cat ${sumbackup} | head -n 1 | tr "," "\n" | grep -nm1 '^startline$' | cut -d: -f1`
	endidx=`cat ${sumbackup} | head -n 1 | tr "," "\n" | grep -nm1 '^endline$' | cut -d: -f1`

	#determine which block entry has largest value in $selectfield (eg: duration) 
	selectblock=`cat ${sumbackup} | tail -n +2 | cut -d, -f${selectidx} \
		| grep -n "" | tr ":" " " | sort -rgk2 | head -n 1 | awk '{print $1}'`

	echo "!!! ${summarylines} recording blocks detected."
	echo "!!! Copying to backup files:"
	echo "!!!     ${ascbackup}"
	echo "!!!     ${sumbackup}"
	echo "!!! Block ${selectblock} automatically selected based on: ${selectfield}"
	echo "!!! Creating new ${ascfile} containing only block ${selectblock}"
	echo

	#print a new .asc file excluding all blocks except the selected one
	#(making sure we retain everything before/after bad blocks)

	rm -f ${ascfile}
	rm -f ${summaryfile}

	curline=1
	for i in `seq 1 $summarylines`; do
		s_line=`cat ${sumbackup} | tail -n +$(( i + 1 )) | head -n 1 | cut -d, -f${startidx}`
		e_line=`cat ${sumbackup} | tail -n +$(( i + 1 )) | head -n 1 | cut -d, -f${endidx}`
		if [[ $i != $selectblock ]]; then
			blockdur=$(( s_line - curline ))
			tail -n +${curline} ${ascbackup} | head -n ${blockdur} >> ${ascfile}
			curline=$(( e_line + 1 ))
		fi
	done	
	tail -n +${curline} ${ascbackup} >> ${ascfile}

	bash ${0} -n ${ascfile}
fi

##########################################################

