#!/bin/bash

#Takes .edf file as input, converts it to .asc in the same directory.
#- Try edf2asc without failsafe, and if there is an error, use failsafe mode
#- First line of output says whether failsafe was used or not
#- Output filename has timestamp removed
#- Scrub header lines containing date information
#- Check for multiple START events in a file.  Need to address these files manually.

inputfile=$1

edfexe=/nrgpackages/tools.release/intradb/EDF_Access_API_linux_x64/Example/edf2asc
#edfexe=./edf2asc
edfexe=`echo $2 $edfexe | awk '{print $1}'`
edf2asc_params="-c"

ascfile=`echo ${inputfile} | sed -E 's/edf$/asc/'`
outfile=`echo ${inputfile} | sed -E 's/(MOV|RET|FIX|REST)eyetrack([0-9]+)_(7T[a-z]?)_.+\.edf$/\3_\1\2_eyetrack.asc/' | sed "s/_FIX/_REST/"`
logfile=`echo ${inputfile} | sed -E 's/.edf$/_edf2asc.log/'`

rm -f ${ascfile} ${outfile} ${logfile}

#try without failsafe first
${edfexe} ${edf2asc_params} ${inputfile} >> ${logfile}

#if it failed, try with failsafe
if [ ! -e ${ascfile} ]; then
	edf2asc_params="-failsafe"
	${edfexe} ${edf2asc_params} ${inputfile} >> ${logfile}
fi

#if still failed, we're out of luck
if [ ! -e ${ascfile} ]; then
	echo "FAILURE: Unable to convert ${inputfile}" | tee -a ${logfile}
	exit 1
fi
numstartlines=`grep -E '^START' ${ascfile} | wc -l`

#start new output with line describing edf2asc parameters (eg: failsafe or no?)
echo "** CONVERTED using "`basename $edfexe`" ${edf2asc_params}" >> ${outfile}

#add edf2asc version information
$edfexe -v | awk '{print "** " $0}' >> ${outfile}

#remove lines containing datestamps (first two lines)
grep -vE '^\*\* (CONVERTED|DATE)' ${ascfile} >> ${outfile}
rm -f ${ascfile}

#if there are 0 or >1 START events (recording blocks), alert the user so they can address the problem
#If there are >1 events, user will need to specify which recording block contains the desired 
# eyetracking data.
if [[ ${numstartlines} != 1 ]]; then
	echo "WARNING: ${numstartlines} START events!" | tee -a ${logfile}
fi

echo "SUCCESS:" `basename ${edfexe}` "${edf2asc_params} ${inputfile} -> `basename ${outfile}`" | tee -a ${logfile}

