function varargout = hcp_behav2xml(matfile)

% function xmlfile = hcp_behav2xml(matfile)
% 
% <matfile> is a string referring to a behavioral .mat file
% <matfile> should be formatted as:
%   [directory/]<subjid>_<scantype><scanidx>_*.mat
%   ex: 123456_MOV1_7T_20150101.mat
%
% This function parses the .mat filename <matfile> to determine scan type, 
% then passes the filename to the appropriate behav parsing function, runs
% a few sanity checks, and outputs a .xml file with the filename:
% <subject>_7T_<scantype><scanidx>_behav.xml

%%%%%%%%%%%%%%% parse filename to determine scan type

[file_dir,file_name,file_ext]=fileparts(matfile);

r=regexp(file_name,'([^/\\]+)_(RET|MOV|FIX|REST)([0-9]+)_','tokens');

if(isempty(r) || ~exist(matfile,'file'))
    error('unable to parse filename: %s', matfile);
end

subj=r{1}{1};
scantype=r{1}{2};
if(strcmp(scantype,'FIX'))
     scantype='REST';
end
scannum=str2num(r{1}{3}); %#ok<ST2NM>

assert(~(isempty(subj) || isempty(scantype) || isempty(scannum)));

outfile=fullfile(file_dir,sprintf('%s_7T_%s%d_behav.xml',subj,scantype,scannum));

%%%%%%%%%%%%%%% sanity check scan index, and assign some expected values
%%%%%%%%%%%%%%% depending on the scan type
switch(scantype)
    case 'RET'
        assert(scannum >= 1 && scannum <= 6);
        output=ret_behav_summary(matfile);

        assert(isfield(output,'expttype'));
        if(scannum <= 4)
            assert(output.expttype==scannum);
        else
            assert(output.expttype==5);
        end
        
        output.ttlCnt = numel(output.ttlStamps);
        expected_ttlCnt = 300;
        %retinotopy stimulus script may miss some TTL triggers, and usually
        %does not record the last TTL, so expect <= 299 sec
        expected_ttl_scandur=[297.9 299.1];
        
    case 'MOV'
        assert(scannum >= 1 && scannum <= 4);
        output=movrest_behav_summary(matfile);
        
        movie_ttlCnt = [921 918 915 901];
        expected_ttlCnt = movie_ttlCnt(scannum);
        expected_ttl_scandur = [expected_ttlCnt-.1 expected_ttlCnt+.1];
        
    case 'REST'
        assert(scannum >= 1 && scannum <= 4);
        output=movrest_behav_summary(matfile);
        expected_ttlCnt = 900;
        expected_ttl_scandur = [expected_ttlCnt-.1 expected_ttlCnt+.1];
        
    otherwise
        
end

num_ttlgap=10;
ttlgap_range = [.9 1.1];

%%%%%%%%%%%%%%%%% Look for irregular stimulus starting times (ie: stimulus
%%%%%%%%%%%%%%%%% was triggered manually instead of waiting for TTL)
if(output.ttlCnt >= num_ttlgap)
    ttlgap=diff(output.ttlStamps(1:num_ttlgap));
    stimulus_onset=1-ttlgap(1);
    
    % Even if first TTL gap is irregular (indicated manual trigger),
    % subsequent TTL gaps should be accurate (within 0.1sec tolerance)
    if(median(ttlgap(2:end)) <= ttlgap_range(1) || median(ttlgap(2:end)) >= ttlgap_range(2))
        error('%s_%s%d TTL2-3 spacing out of range: %.3f',subj,scantype,scannum,ttlgap(2));
    end

    if(ttlgap(1) <= ttlgap_range(1) || ttlgap(1) >= ttlgap_range(2))
        %error('%s_%s%d TTL1-2 spacing out of range: %.3f',subj,scantype,scannum,ttlgap(1));
        %warning('%s_%s%d TTL1-2 spacing out of range: %.3f',subj,scantype,scannum,ttlgap(1));
    end
    
else
    % If no TTLs were recorded, we cannot estimate stimulus onset
    stimulus_onset=nan;
end

%%%%%%%%%%%%%%%%% sanity check scan duration
ttl_scandur=output.ttlStamps(end)-output.ttlStamps(1);

if(numel(expected_ttlCnt) == 1 && output.ttlCnt ~= expected_ttlCnt)
    
    %Stimulus script may miss some TTL, but if the time between first TTL
    %and last is correct, we are still OK.
    if(ttl_scandur <= expected_ttl_scandur(1) || ttl_scandur >= (expected_ttl_scandur(2)))
        error('%s_%s%d Incorrect TTL count: %d, %.3fsec (expected %d)',subj,scantype,scannum,output.ttlCnt,ttl_scandur,expected_ttlCnt);
    end
end

%%%%%%%%%%%%%%%% add additional fields to output

output.subject=subj;
output.description=sprintf('%s%d',scantype,scannum);
output.stimulus_onset=stimulus_onset;

if(nargout == 1)
    varargout = {outfile};
elseif(nargout == 2)
    varargout = {outfile, output};
end

struct2xml(struct('behav_summary',output),outfile);
